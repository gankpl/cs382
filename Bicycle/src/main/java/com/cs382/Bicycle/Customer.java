package com.cs382.Bicycle;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Customer {
	@Column(nullable=false)
	private String citizenID;
	@Column(nullable=false)
	private String firstName;
	@Column(nullable=false)
	private String lastName;
	
	public void setCitizenID(String input){
		this.citizenID = input;
	}
	
	public void setFirstName(String fname){
		this.firstName = fname;
	}
	
	public void setLastName(String lname){
		this.lastName = lname;
	}
	public String getCitizenID(){
		return this.citizenID;
	}
	public String getFirstName(){
		return this.firstName;
	}
	public String getLastName(){
		return this.lastName;
	}
}
