package com.cs382.Bicycle;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class RegisterController {
	@RequestMapping(method = RequestMethod.GET)
	public String printHello(ModelMap model){
		model.addAttribute("message","Hello");
		return "register";
	}
	
	@RequestMapping(value="/addMember" , method=RequestMethod.POST)
	public String addMember(@ModelAttribute("registerForm") Customer customer){
		System.out.println("ID: "+customer.getCitizenID());
		System.out.println("FNAME: "+customer.getFirstName());
		System.out.println("LNAME: "+customer.getLastName());
		if(customer.getCitizenID()=="" || customer.getFirstName()=="" || customer.getLastName()==""){
			return "register-error";
		}
		else{
			return "register-result";
		}
	}
	

}
